<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/naujienos', 'NewsController@index');

Route::get('/labas', 'HomeController@index');

Route::get('/about', function () {
	return view('contacts');
});

Route::get('/skaiciuokle', function () {
    return view('skaiciuokle');
});
//////////////////////////////////////////////////////
Route::post('/suma', 'HomeController@suma')->name('suma');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


