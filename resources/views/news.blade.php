@extends("layouts.app")

@section('content')
<div class="container">
	<h1>Naujienos</h1>

	<!-- Einame per visa naujienu masyva gauta is newsController -->
	@foreach($news as $newsItem)
		<div>
			<h3>
				<!-- spausdiname naujienos pavadinima -->
				{{ $newsItem->title }}
			</h3>
			<p>
				{{ $newsItem->content }}
			</p>
		</div>
	@endforeach
</div>
@endsection

@section('sidebar')
	cia yra sidebar
@endsection